var gulp = require('gulp');
var dedupe = require('gulp-dedupe');
var concatCss = require('gulp-concat-css');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');


 
gulp.task('concat', function () {
  return gulp.src('css_edit/*.css')
   .pipe(dedupe()) 
    .pipe(concatCss("bundle_final.css"))
    .pipe(gulp.dest('css_final/'));
});


 
gulp.task('autoprefixer', () =>
    gulp.src('css_edit/*.css')
       .pipe(autoprefixer({browsers: ['last 2 versions'],cascade: true}))
    	// .pipe(gulp.dest('out/'));
);
 
gulp.task('minify-css', function() {
  return gulp.src('css_edit/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('out/'));
});

// js files

 gulp.task('js-comp', function () {
   return gulp.src(['file1.js', 'file2.js', 'file3.js'])
     .pipe(gp_concat('concat.js'))
     .pipe(gp_uglify())
     .pipe(gulp.dest('js'));
 });




gulp.task('default', ['autoprefixer', 'concat', 'minify-css', 'js-comp']);
