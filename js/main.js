(function ($) {
    "use strict";

    jQuery(document).ready(function ($) {


        //Menu Smooth-Scroll
		var smoothscroll = $('.smooth-scroll a'),
		  		scrollTrigger = $('.scrollTop a');
        smoothscroll.on('click', function () {
            $(".active").removeClass("active");
            $(this).closest('li').addClass("active");
            var theClass = $(this).attr("class");
            $('.' + theClass).parent('li').addClass('active');
            $('html, body').stop().animate({
                scrollTop: $($(this).attr('href')).offset().top - 100
            }, 1000);
            return false;
        });
        scrollTrigger.scrollTop();


        //jQuery scroll top
        $.fn.scrollToTop = function () {
            $(this).hide();

            if ($(window).scrollTop() !== 0 ) {
                $(this).fadeIn("slow");
            }

            var scrollDiv = $(this);
            $(window).scroll(function () {
                if ($(window).scrollTop() === 0 ) {
                    $(scrollDiv).fadeOut("slow");
                } else {
                    $(scrollDiv).fadeIn("slow");
                }
            });

            $(this).on('click', function () {
                $("html, body").animate({
                        scrollTop: 0
                    },
                    "slow"
                );
            });
        };
        $("#toTop").scrollToTop();



        //Bootstrap Menu
		  var navA = $('.nav.navbar-nav li a'),
		  		navColapse = $('.navbar-collapse');
        navA.on('click', function () {
            navColapse.removeClass('in');
        });

        // jQuery scroll psy
        $('body').scrollspy({
            target: '.navbar-collapse',
            offset: 95
        });

        if ($.fn.slick) {

			  	var slideWrap = $('.slider-wrap'),
					slideWrap2 = $('.slider-wrap-2'),
					slideWrap3 = $('.slider-wrap-3'),
					reviewsWrap = $('.reviews-wrap'),
                    blogThumbWrap = $('.blog-slides-thumbnail');


            //Slider Activated
            slideWrap.slick({
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                vertical: true,
                verticalSwiping: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 10,
                prevArrow: '<i class="icofont BslideNav icofont-long-arrow-left"></i>',
                nextArrow: '<i class="icofont BslideNav icofont-long-arrow-right"></i>'
            });

            //Slider - 2 Activated
            slideWrap2.slick({
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 1000,
                prevArrow: '<i class="icofont BslideNav icofont-long-arrow-left"></i>',
                nextArrow: '<i class="icofont BslideNav icofont-long-arrow-right"></i>'
            });

            //Slider - 3 Activated
            slideWrap3.slick({
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                fade: true,
                speed: 1000,
                prevArrow: '<i class="icofont BslideNav icofont-long-arrow-left"></i>',
                nextArrow: '<i class="icofont BslideNav icofont-long-arrow-right"></i>'
            });

            //Reviews Slider Activated
            reviewsWrap.slick({
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                draggable: true,
                autoplay: true, /* this is the new line */
                autoplaySpeed: 2000,
                infinite: true,
               
                touchThreshold: 1000,
            });

            //Reviews Slider Activated
            blogThumbWrap.slick({
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                drag:true,
                prevArrow:'<i class="icofont s-navigation icofont-simple-left"></i>',
                nextArrow:'<i class="icofont s-navigation icofont-simple-right"></i>',
                draggable: true,
                autoplay: true, /* this is the new line */
                autoplaySpeed: 2000,
                infinite: true,
                touchThreshold: 1000,
            });

        }


        
        //Skills Progressbar
        if ($.fn.barfiller) {
			  	var bar1 = $('#bar1'),
					 bar2 = $('#bar2'),
					 bar3 = $('#bar3'),
					 bar4 = $('#bar4');


            bar1.barfiller({
                barColor: '#5C97BF'
            });
            bar2.barfiller({
                barColor: '#ffc000'
            });
            bar3.barfiller({
                barColor: '#fc7f0c'
            });
            bar4.barfiller({
                barColor: '#e84b3a'
            });

        }

        //CounterUp Activated
        if ($.fn.counterUp) {

            $('.count').counterUp({
                delay: 10,
                time: 3000
            });

        }


        if ($.fn.sticky) {
            $("#header-area").sticky({
                topSpacing: 0
            });
        }

    });





    //Preloader JS
    $(window).on('load', function () {
        $('.preloader').fadeOut('slow', function () {
            $(this).remove();
        });
    });


}(jQuery));


// scroll reveal js scripts

window.sr = ScrollReveal();
sr.reveal('.section-title', {
    duration:1000,
    origin:'top',
    distance:'10px'
});
sr.reveal('.about-text', {
    duration:2000,
    origin:'left',
    distance:'100px'
});
sr.reveal('.course_card', {
    duration:2000,
    origin:'bottom',
    distance:'100px'
});
 
sr.reveal('.aboutus-area img', {
    duration:2000,
    origin:'bottom',
    distance:'100px'
});
sr.reveal('.facts-section', {
    duration:2000,
    origin:'bottom',
    // distance:'100px'
});
sr.reveal('.slider-area', {
    duration: 2000,
        origin: 'bottom',
    // distance:'100px'
});
sr.reveal('.form-holder', {
    duration: 2000,
        origin: 'bottom',
    // distance:'100px'
});
 
sr.reveal('.contact_container', {
    duration: 2000,
        origin: 'bottom',
    // distance:'100px'
});
 
sr.reveal('.footer-bottom', {
    duration: 2000,
        origin: 'top',
    // distance:'100px'
});
 